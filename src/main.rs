use comrak::{markdown_to_html, ComrakOptions};
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;
use structopt::StructOpt;

///  
/// Parses a MARKDOWN file to HTML, adding a link to `style.css`  
///
/// `ComrakOptions::unsafe_` is enabled for rendering raw HTML.  
#[derive(Debug, StructOpt)]
struct Cli {
    /// /path/to/your/file.md
    #[structopt(parse(from_os_str))]
    input_file: PathBuf,
}

fn main() {
    let cli = Cli::from_args();
    let contents = import(&cli.input_file).expect("Err from import.");
    parse(contents).expect("Err from parse.");
}

fn import(input: &PathBuf) -> std::io::Result<String> {
    let file = File::open(input)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    let mut raw = String::from("<!DOCTYPE html><link href=\"style.css\" rel=\"stylesheet\"></link>\n");
    raw.push_str(&contents);
    Ok(raw)
}

fn parse(md: String) -> std::io::Result<()> {
    let mut file = File::create("index.html")?;
    let options = ComrakOptions{
        unsafe_: true,
        ..ComrakOptions::default()
    };
    let html = markdown_to_html(&md, &options);
    let html = &html.as_bytes();
    file.write(html)?;
    Ok(())
}
