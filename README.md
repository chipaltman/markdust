# Markdust  
This is a learning project, written in Rust. 

```
Parses a MARKDOWN file to HTML, adding a link to `style.css` `ComrakOptions::unsafe_` is enabled for rendering raw HTML.

USAGE:
    markdust <input_file>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <input_file>    /path/to/your/file.md
```
